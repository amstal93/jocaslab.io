---
title: "GraalVM + Clojure + http-kit"
---

# GraalVM + Clojure + http-kit

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**

- [Native compilation + Clojure](#native-compilation--clojure)
- [Gotchas](#gotchas)
  - [Namespaces](#namespaces)
  - [Native compilation](#native-compilation)
  - [`http-kit` with Server Name Indication (SNI)](#http-kit-with-server-name-indication-sni)
- [Results](#results)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Native compilation + Clojure

Many examples for a combination of GraalVM and Clojure have been done [already](https://janstepien.com/native-clojure-with-graalvm/). Since GraalVM has limitations (and GraalVM is still a Release Candidate RC) some configurations might be a bit trickier to get up and running than others. In this post I'll create a command line utility that prints the Elasticsearch status.

The source code can be found [here](https://gitlab.com/Jocas/http-kit-native).

## Gotchas

In this case it was not that easy to get the native executable as just running `clj -A:native-image`. This section of the text is dedicated to the poor Clojure enthusiasts that wants to compile a native executable with `http-kit` dependency.

### Namespaces

For the native compilation namespace names must be provided with underscores not with dashes:
```
{:deps    {org.clojure/clojure {:mvn/version "1.10.0"}
           http-kit            {:mvn/version "2.3.0"}}
 :paths   ["src"]
 :aliases {:native-image
           {:extra-deps {luchiniatwork/cambada {:mvn/version "1.0.0"}}
            :main-opts  ["-m" "cambada.native-image"
                         "-m" "es_utils.core"]}}}
```

The important namespace name is this one: `es_utils.core`. If provided like `es-utils.core` the native executable compilation will just hang. RC.

### Native compilation

Let's try to compile;

```
$ clj -A:native-image
Cleaning target
Creating target/classes
  Compiling es-utils.core
Creating target/http-kit-native
ERROR! error: unsupported features in 2 methods
Detailed message:
Error: com.oracle.graal.pointsto.constraints.UnsupportedFeatureException: No instances are allowed in the image heap for a class that is initialized or reinitialized at image runtime: javax.net.ssl.SSLContext
Trace: 
        at parsing org.httpkit.client.HttpClient.exec(HttpClient.java:347)
Call path from entry point to org.httpkit.client.HttpClient.exec(String, RequestConfig, SSLEngine, IRespListener): 
        at org.httpkit.client.HttpClient.exec(HttpClient.java:269)
        at org.httpkit.client$request.invokeStatic(client.clj:258)
        at org.httpkit.client$request.doInvoke(client.clj:152)
        at clojure.lang.RestFn.applyTo(RestFn.java:139)
        at es_utils.core.main(Unknown Source)
        at com.oracle.svm.core.JavaMainWrapper.run(JavaMainWrapper.java:164)
        at com.oracle.svm.core.code.IsolateEnterStub.JavaMainWrapper_run_5087f5482cc9a6abc971913ece43acb471d2631b(generated:0)
Error: com.oracle.graal.pointsto.constraints.UnsupportedFeatureException: No instances are allowed in the image heap for a class that is initialized or reinitialized at image runtime: javax.net.ssl.SSLContext
Trace: 
        at parsing org.httpkit.client.SslContextFactory.trustAnybody(SslContextFactory.java:36)
Call path from entry point to org.httpkit.client.SslContextFactory.trustAnybody(): 
        at org.httpkit.client.SslContextFactory.trustAnybody(SslContextFactory.java:36)
        at org.httpkit.client$coerce_req.invokeStatic(client.clj:68)
        at org.httpkit.client$coerce_req.invoke(client.clj:59)
        at clojure.lang.AFn.applyToHelper(AFn.java:154)
        at clojure.lang.RestFn.applyTo(RestFn.java:132)
        at es_utils.core.main(Unknown Source)
        at com.oracle.svm.core.JavaMainWrapper.run(JavaMainWrapper.java:164)
        at com.oracle.svm.core.code.IsolateEnterStub.JavaMainWrapper_run_5087f5482cc9a6abc971913ece43acb471d2631b(generated:0)

Error: Image building with exit status 1
```
Error `No instances are allowed in the image heap for a class that is initialized or reinitialized at image runtime`.

To fix this issue we need to instruct GraalVM compiler to rerun class initialization at the run time.

```
{:deps    {org.clojure/clojure {:mvn/version "1.10.0"}
           http-kit            {:mvn/version "2.3.0"}}
 :paths   ["src"]
 :aliases {:native-image
           {:extra-deps {luchiniatwork/cambada {:mvn/version "1.0.0"}}
            :main-opts  ["-m" "cambada.native-image"
                         "-m" "es_utils.core"
                         "-O" "-rerun-class-initialization-at-runtime=org.httpkit.client.SslContextFactory"
                         "-O" "-rerun-class-initialization-at-runtime=org.httpkit.client.HttpClient"]}}}

```

Let's rerun native executable compilation:

```
 clj -A:native-image
Cleaning target
Creating target/classes
  Compiling es-utils.core
Creating target/http-kit-native
[target/http-kit-native:23409]    classlist:   3,500.56 ms
[target/http-kit-native:23409]        (cap):   1,434.20 ms
[target/http-kit-native:23409]        setup:   3,472.52 ms
[target/http-kit-native:23409]   (typeflow):  11,331.00 ms
[target/http-kit-native:23409]    (objects):   5,717.03 ms
[target/http-kit-native:23409]   (features):     245.60 ms
[target/http-kit-native:23409]     analysis:  17,593.05 ms
[target/http-kit-native:23409]     universe:     420.37 ms
[target/http-kit-native:23409]      (parse):   2,386.01 ms
[target/http-kit-native:23409]     (inline):   2,540.30 ms
[target/http-kit-native:23409]    (compile):  16,718.56 ms
[target/http-kit-native:23409]      compile:  22,383.70 ms
[target/http-kit-native:23409]        image:   1,497.00 ms
[target/http-kit-native:23409]        write:     282.81 ms
[target/http-kit-native:23409]      [total]:  49,247.55 ms

Done!
```

Great, let's try to run the compiled executable:
```
$ ./target/http-kit-native 
Exception in thread "main" java.lang.Error: Failed to initialize SSLContext
        at org.httpkit.client.HttpClient.<clinit>(HttpClient.java:41)
        at com.oracle.svm.core.hub.ClassInitializationInfo.invokeClassInitializer(ClassInitializationInfo.java:341)
        at com.oracle.svm.core.hub.ClassInitializationInfo.initialize(ClassInitializationInfo.java:261)
        at java.lang.Class.ensureInitialized(DynamicHub.java:375)
        at org.httpkit.client$fn__1737$fn__1738.invoke(client.clj:97)
        at clojure.lang.Delay.deref(Delay.java:42)
        at clojure.core$deref.invokeStatic(core.clj:2320)
        at clojure.core$deref.invoke(core.clj:2306)
        at org.httpkit.client$request.invokeStatic(client.clj:212)
        at org.httpkit.client$request.doInvoke(client.clj:152)
        at clojure.lang.RestFn.invoke(RestFn.java:410)
        at es_utils.core$_main.invokeStatic(core.clj:7)
        at es_utils.core$_main.doInvoke(core.clj:5)
        at clojure.lang.RestFn.invoke(RestFn.java:397)
        at clojure.lang.AFn.applyToHelper(AFn.java:152)
        at clojure.lang.RestFn.applyTo(RestFn.java:132)
        at es_utils.core.main(Unknown Source)
Caused by: java.security.NoSuchAlgorithmException: Error constructing implementation (algorithm: Default, provider: SunJSSE, class: sun.security.ssl.SSLContextImpl$DefaultSSLContext)
        at java.security.Provider$Service.newInstance(Provider.java:1621)
        at sun.security.jca.GetInstance.getInstance(GetInstance.java:236)
        at sun.security.jca.GetInstance.getInstance(GetInstance.java:164)
        at javax.net.ssl.SSLContext.getInstance(SSLContext.java:156)
        at javax.net.ssl.SSLContext.getDefault(SSLContext.java:96)
        at org.httpkit.client.HttpClient.<clinit>(HttpClient.java:39)
        ... 16 more
Caused by: java.lang.NoSuchMethodException: sun.security.ssl.SSLContextImpl$DefaultSSLContext.<init>()
        at java.lang.Class.getConstructor0(DynamicHub.java:3082)
        at java.lang.Class.getConstructor(DynamicHub.java:1825)
        at java.security.Provider$Service.newInstance(Provider.java:1594)
        ... 21 more
```
Oh Snap!

It turns out that GraalVM needs to be instructed to enable security services with a flag `--enable-all-security-services`.

```
{:deps    {org.clojure/clojure {:mvn/version "1.10.0"}
           http-kit            {:mvn/version "2.3.0"}}
 :paths   ["src"]
 :aliases {:native-image
           {:extra-deps {luchiniatwork/cambada {:mvn/version "1.0.0"}}
            :main-opts  ["-m" "cambada.native-image"
                         "-m" "es_utils.core"
                         "-O" "-enable-all-security-services"
                         "-O" "-rerun-class-initialization-at-runtime=org.httpkit.client.SslContextFactory"
                         "-O" "-rerun-class-initialization-at-runtime=org.httpkit.client.HttpClient"]}}}
```

```
clj -A:native-image                       
Cleaning target
Creating target/classes
  Compiling es-utils.core
Creating target/http-kit-native
[target/http-kit-native:23888]    classlist:   2,950.13 ms
[target/http-kit-native:23888]        (cap):   1,252.14 ms
[target/http-kit-native:23888]        setup:   3,405.92 ms
[target/http-kit-native:23888]   (typeflow):  18,867.55 ms
[target/http-kit-native:23888]    (objects):  12,818.21 ms
[target/http-kit-native:23888]   (features):     643.97 ms
[target/http-kit-native:23888]     analysis:  32,900.23 ms
[target/http-kit-native:23888]     universe:     738.95 ms
[target/http-kit-native:23888]      (parse):   4,474.64 ms
[target/http-kit-native:23888]     (inline):   4,284.29 ms
[target/http-kit-native:23888]    (compile):  29,974.53 ms
[target/http-kit-native:23888]      compile:  40,073.32 ms
[target/http-kit-native:23888]        image:   2,494.85 ms
[target/http-kit-native:23888]        write:     296.17 ms
[target/http-kit-native:23888]      [total]:  82,982.94 ms

Done!
```

Let's run the native executable:

```
$ ./target/http-kit-native                  
WARNING: The sunec native library, required by the SunEC provider, could not be loaded. This library is usually shipped as part of the JDK and can be found under <JAVA_HOME>/jre/lib/<platform>/libsunec.so. It is loaded at run time via System.loadLibrary("sunec"), the first time services from SunEC are accessed. To use this provider's services the java.library.path system property needs to be set accordingly to point to a location that contains libsunec.so. Note that if java.library.path is not set it defaults to the current working directory.
{:opts {:method :get, :headers {Content-Type application/json}, :url http://localhost:9200}, :body {
  "name" : "-yc7hn8",
  "cluster_name" : "docker-cluster",
  "cluster_uuid" : "xLBZAUvITWefFRGCM0IpZw",
  "version" : {
    "number" : "6.3.1",
    "build_flavor" : "oss",
    "build_type" : "tar",
    "build_hash" : "eb782d0",
    "build_date" : "2018-06-29T21:59:26.107521Z",
    "build_snapshot" : false,
    "lucene_version" : "7.3.1",
    "minimum_wire_compatibility_version" : "5.6.0",
    "minimum_index_compatibility_version" : "5.0.0"
  },
  "tagline" : "You Know, for Search"
}
, :headers {:content-encoding gzip, :content-length 297, :content-type application/json; charset=UTF-8}, :status 200}
```

Great! But still we have a warning
```
WARNING: The sunec native library, required by the SunEC provider, could not be loaded. This library is usually shipped as part of the JDK and can be found under <JAVA_HOME>/jre/lib/<platform>/libsunec.so. It is loaded at run time via System.loadLibrary("sunec"), the first time services from SunEC are accessed. To use this provider's services the java.library.path system property needs to be set accordingly to point to a location that contains libsunec.so. Note that if java.library.path is not set it defaults to the current working directory.
```

Let's try to use a link with HTTPS:
```
./target/http-kit-native https://some-elasticsearch-instance.eu-west-1.es.amazonaws.com
WARNING: The sunec native library, required by the SunEC provider, could not be loaded. This library is usually shipped as part of the JDK and can be found under <JAVA_HOME>/jre/lib/<platform>/libsunec.so. It is loaded at run time via System.loadLibrary("sunec"), the first time services from SunEC are accessed. To use this provider's services the java.library.path system property needs to be set accordingly to point to a location that contains libsunec.so. Note that if java.library.path is not set it defaults to the current working directory.
Sun Feb 03 23:12:48 EET 2019 [client-loop] ERROR - select exception, should not happen
java.lang.UnsatisfiedLinkError: sun.security.ec.ECKeyPairGenerator.generateECKeyPair(I[B[B)[Ljava/lang/Object; [symbol: Java_sun_security_ec_ECKeyPairGenerator_generateECKeyPair or Java_sun_security_ec_ECKeyPairGenerator_generateECKeyPair__I_3B_3B]
        at com.oracle.svm.jni.access.JNINativeLinkage.getOrFindEntryPoint(JNINativeLinkage.java:145)
        at com.oracle.svm.jni.JNIGeneratedMethodSupport.nativeCallAddress(JNIGeneratedMethodSupport.java:54)
        at sun.security.ec.ECKeyPairGenerator.generateECKeyPair(ECKeyPairGenerator.java)
        at sun.security.ec.ECKeyPairGenerator.generateKeyPair(ECKeyPairGenerator.java:128)
        at java.security.KeyPairGenerator$Delegate.generateKeyPair(KeyPairGenerator.java:703)
        at sun.security.ssl.ECDHCrypt.<init>(ECDHCrypt.java:78)
        at sun.security.ssl.ClientHandshaker.serverKeyExchange(ClientHandshaker.java:783)
        at sun.security.ssl.ClientHandshaker.processMessage(ClientHandshaker.java:302)
        at sun.security.ssl.Handshaker.processLoop(Handshaker.java:1037)
        at sun.security.ssl.Handshaker$1.run(Handshaker.java:970)
        at sun.security.ssl.Handshaker$1.run(Handshaker.java:967)
        at java.security.AccessController.doPrivileged(AccessController.java:97)
        at sun.security.ssl.Handshaker$DelegatedTask.run(Handshaker.java:1459)
        at org.httpkit.client.HttpsRequest.doHandshake(HttpsRequest.java:89)
        at org.httpkit.client.HttpClient.doRead(HttpClient.java:177)
        at org.httpkit.client.HttpClient.run(HttpClient.java:474)
        at java.lang.Thread.run(Thread.java:748)
        at com.oracle.svm.core.thread.JavaThreads.threadStartRoutine(JavaThreads.java:480)
        at com.oracle.svm.core.posix.thread.PosixJavaThreads.pthreadStartRoutine(PosixJavaThreads.java:198)
{:opts {:method :get, :headers {Content-Type application/json}, :url https://some-elasticsearch-instance.eu-west-1.es.amazonaws.com}, :error #error {
 :cause idle timeout: 60000ms
 :via
 [{:type org.httpkit.client.TimeoutException
   :message idle timeout: 60000ms
   :at [org.httpkit.client.HttpClient clearTimeout HttpClient.java 120]}]
 :trace
 [[org.httpkit.client.HttpClient clearTimeout HttpClient.java 120]
  [org.httpkit.client.HttpClient run HttpClient.java 481]
  [java.lang.Thread run Thread.java 748]
  [com.oracle.svm.core.thread.JavaThreads threadStartRoutine JavaThreads.java 480]
  [com.oracle.svm.core.posix.thread.PosixJavaThreads pthreadStartRoutine PosixJavaThreads.java 198]]}}

```

Wow! We have an exception!

Let's try to get rid of this warning. To do that we need to copy the `libsunec.so` file to the current directory.

```
$ cp $GRAALVM_HOME/jre/lib/amd64/libsunec.so . 
```

Run the native executable once again:
```
$ ./target/http-kit-native https://some-elasticsearch-instance.eu-west-1.es.amazonaws.com
{:opts {:method :get, :headers {Content-Type application/json}, :url https://some-elasticsearch-instance.eu-west-1.es.amazonaws.com}, :body {
  "name" : "NukC0WZ",
  "cluster_name" : "cluster-name",
  "cluster_uuid" : "a6YnhvMkT_mqNwyh6pL21A",
  "version" : {
    "number" : "6.3.1",
    "build_flavor" : "oss",
    "build_type" : "zip",
    "build_hash" : "e6df130",
    "build_date" : "2018-08-01T07:23:36.254471Z",
    "build_snapshot" : false,
    "lucene_version" : "7.3.1",
    "minimum_wire_compatibility_version" : "5.6.0",
    "minimum_index_compatibility_version" : "5.0.0"
  },
  "tagline" : "You Know, for Search"
}
, :headers {:access-control-allow-origin *, :connection keep-alive, :content-length 517, :content-type application/json; charset=UTF-8, :date Sun, 03 Feb 2019 21:10:49 GMT}, :status 200}
```

Works!

### `http-kit` with Server Name Indication (SNI)

For this particular application we don't really need it but let's add SNI support for `http-kit`.

As described [here](https://medium.com/@kumarshantanu/using-server-name-indication-sni-with-http-kit-client-f7d92954e165).

```
(ns es-utils.core
  (:gen-class)
  (:require [org.httpkit.client :as http])
  (:import [java.net URI]
           [javax.net.ssl SNIHostName SSLEngine SSLParameters]))

(defn sni-configure
  [^SSLEngine ssl-engine ^URI uri]
  (let [^SSLParameters ssl-params (.getSSLParameters ssl-engine)]
    (.setServerNames ssl-params [(SNIHostName. (.getHost uri))])
    (.setSSLParameters ssl-engine ssl-params)))

(def sni-client (http/make-client {:ssl-configurer sni-configure}))

(defn -main [& args]
  (println
    @(http/request
       {:method  :get
        :client sni-client
        :headers {"Content-Type" "application/json"}
        :url     (or (first args)"http://localhost:9200")})))
```

And build native executable:

```
$ clj -A:native-image                                                                                                       
Cleaning target
Creating target/classes
  Compiling es-utils.core
Creating target/http-kit-native
ERROR! error: No instances are allowed in the image heap for a class that is initialized or reinitialized at image runtime: org.httpkit.client.HttpClient
Detailed message:
Error: No instances are allowed in the image heap for a class that is initialized or reinitialized at image runtime: org.httpkit.client.HttpClient
Trace:  object clojure.lang.Var
        method es_utils.core$_main.invokeStatic(ISeq)
Call path from entry point to es_utils.core$_main.invokeStatic(ISeq): 
        at es_utils.core$_main.invokeStatic(core.clj:15)
        at es_utils.core$_main.doInvoke(core.clj:15)
        at clojure.lang.RestFn.applyTo(RestFn.java:137)
        at es_utils.core.main(Unknown Source)
        at com.oracle.svm.core.JavaMainWrapper.run(JavaMainWrapper.java:164)
        at com.oracle.svm.core.code.IsolateEnterStub.JavaMainWrapper_run_5087f5482cc9a6abc971913ece43acb471d2631b(generated:0)

Error: Image building with exit status 1
```

GraalVM compiler complains that the `org.httpkit.client.HttpClient` class is initialized or reinitialized at image run time. This is because of the param `--rerun-class-initialization-at-runtime=org.httpkit.client.HttpClient` that we passed to the GraalVM at build time while we are using `def` in out code that initialized that class during compile time.

To fix the issue we can put the `sni-client` initialization to the `delay` block like this:

```
(ns es-utils.core
  (:gen-class)
  (:require [org.httpkit.client :as http])
  (:import [java.net URI]
           [javax.net.ssl SNIHostName SSLEngine SSLParameters]))

(defn sni-configure
  [^SSLEngine ssl-engine ^URI uri]
  (let [^SSLParameters ssl-params (.getSSLParameters ssl-engine)]
    (.setServerNames ssl-params [(SNIHostName. (.getHost uri))])
    (.setSSLParameters ssl-engine ssl-params)))

(def sni-client (delay (http/make-client {:ssl-configurer sni-configure})))

(defn -main [& args]
  (println
    @(http/request
       {:method  :get
        :client  @sni-client
        :headers {"Content-Type" "application/json"}
        :url     (or (first args) "http://localhost:9200")})))
```

Compile:
```
$ clj -A:native-image             
Cleaning target
Creating target/classes
  Compiling es-utils.core
Creating target/http-kit-native
[target/http-kit-native:25199]    classlist:   2,965.95 ms
[target/http-kit-native:25199]        (cap):   1,168.37 ms
[target/http-kit-native:25199]        setup:   3,223.32 ms
[target/http-kit-native:25199]   (typeflow):  20,225.61 ms
[target/http-kit-native:25199]    (objects):  13,665.09 ms
[target/http-kit-native:25199]   (features):     905.32 ms
[target/http-kit-native:25199]     analysis:  35,411.51 ms
[target/http-kit-native:25199]     universe:     718.22 ms
[target/http-kit-native:25199]      (parse):   4,455.65 ms
[target/http-kit-native:25199]     (inline):   4,589.87 ms
[target/http-kit-native:25199]    (compile):  28,783.97 ms
[target/http-kit-native:25199]      compile:  39,313.46 ms
[target/http-kit-native:25199]        image:   2,066.92 ms
[target/http-kit-native:25199]        write:     305.70 ms
[target/http-kit-native:25199]      [total]:  84,125.35 ms

Done!
```

## Results

We've shown that it is possible to compile a simple Clojure utility to a native executable. All this necessary and complicated machinery provides the benefits of faster startup time (of course, compared to vanilla Clojure) and reduced usage of memory and CPU.

Let's run our native executable and measure time:
```
$ time ./target/http-kit-native                   
{:opts {:method :get, :client #object[org.httpkit.client.HttpClient 0x71fb17f8 org.httpkit.client.HttpClient], :headers {Content-Type application/json}, :url http://localhost:9200}, :body {
  "name" : "-yc7hn8",
  "cluster_name" : "docker-cluster",
  "cluster_uuid" : "xLBZAUvITWefFRGCM0IpZw",
  "version" : {
    "number" : "6.3.1",
    "build_flavor" : "oss",
    "build_type" : "tar",
    "build_hash" : "eb782d0",
    "build_date" : "2018-06-29T21:59:26.107521Z",
    "build_snapshot" : false,
    "lucene_version" : "7.3.1",
    "minimum_wire_compatibility_version" : "5.6.0",
    "minimum_index_compatibility_version" : "5.0.0"
  },
  "tagline" : "You Know, for Search"
}
, :headers {:content-encoding gzip, :content-length 297, :content-type application/json; charset=UTF-8}, :status 200}
./target/http-kit-native  0.01s user 0.02s system 89% cpu 0.035 total
```
The total time is 0.035 seconds!

While running the same utility via clj:
```
time clj -m es-utils.core
```
It takes 2.194 seconds.

The compiled executable weights 19 MB.

The downside is that compilation takes around 84 seconds on Ubuntu 18.10 with Intel® Core™ i5-6300U CPU @ 2.40GHz × 4  with 16 GB of RAM and SSD. Compilation is also RAM hungry: ~3.7 GB was used.
